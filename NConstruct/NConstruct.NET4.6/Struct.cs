﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NConstruct
{
    /// <summary>
    /// A construct that acts as a container for other constructs, like the C struct
    /// </summary>
    public class Struct : Construct
    {
        private Dictionary<uint, string> _indexed;
        private Dictionary<string, Construct> _children;

        public override object Value
        {
            get => _children;
            set { }
        }

        public Struct(string name, params Construct[] subConstructs) : base(name)
        {
            _indexed = new Dictionary<uint, string>();
            _children = new Dictionary<string, Construct>();
            
            for (uint i = 0; i < subConstructs.Length; i++)
            {
                _indexed.Add(i, subConstructs[i].Name);
                _children.Add(subConstructs[i].Name, subConstructs[i]);
            }
        }

        public T Get<T>(string name) where T : Construct
        {
            return (T)_children[name];
        }

        public T Get<T>(uint index) where T : Construct
        {
            return (T)_children[_indexed[index]];
        }

        public T GetValue<T>(string name)
        {
            return (T)_children[name].Value;
        }

        public T GetValue<T>(uint index)
        {
            return (T)_children[_indexed[index]].Value;
        }

        public void SetValue<T>(string name, T value)
        {
            _children[name].Value = value;
        }

        public void SetValue<T>(uint index, T value)
        {
            _children[_indexed[index]].Value = value;
        }

        public override void Parse(byte[] data, ref int iterator)
        {
            foreach(var key in _indexed.Keys)
            {
                _children[_indexed[key]].Parse(data, ref iterator);
            }
        }

        public override void Parse(Stream stream)
        {
            foreach (var key in _indexed.Keys)
            {
                _children[_indexed[key]].Parse(stream);
            }
        }
    }
}