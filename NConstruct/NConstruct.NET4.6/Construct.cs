﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NConstruct
{
    /// <summary>
    /// Most basic type for a construct, to be inherited by other classes 
    /// </summary>
    public abstract class Construct
    {
        public string Name { get; private set; }
        public abstract object Value { get; set; }

        public Construct(string name)
        {
            Name = name;
        }

        public Construct(string name, object value) : this(name)
        {
            Value = value;
        }

        public abstract void Parse(byte[] data, ref int iterator);

        public abstract void Parse(Stream stream);
    }
}
